# Cryptocurrency Arbitrage Trader - Mercury

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [History](#history)
- [What is it](#what-is-it)
- [Installation and Configuration](#installation-and-configuration)
- [General logic of the application](#general-logic-of-the-application)
- [Example](#example)
- [Logs](#logs)
- [Webserver](#webserver)
- [ATM](#atm)
  - [*Stage 1. ATM_Evaluate*](#stage-1-atm_evaluate)
  - [*Stage 2. ATM_Prioritize*](#stage-2-atm_prioritize)
  - [*Stage 3. ATM_Execute*](#stage-3-atm_execute)
    - [ATM_Execute_Instant](#atm_execute_instant)
    - [ATM_Execute_Limit](#atm_execute_limit)
- [Manual trading](#manual-trading)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## History
Cryptocurrency Arbitrage Trader - Mercury

This the code of project codename *Mercury* developed mainly in 2017-2018 by Pablo Criado-Pérez (@pcriadoperez) and Alejandro Criado-Pérez (@criadoperez).

Code was initally developed in another local SVN repository and migrated to Gitlab in 2021.

We are not responsable for any financial gains or losses you could encounter by using this software. Please use responsably and be sure to know what you are doing.

## What is it
This is a cryptocurrency arbitrage trader. It's intented to **detect** arbitrage opportunities and optionally **trade** automatically for you.

To **arbitrage** is to buy and sell the same cryptocurrenty at the same time in different markets when they trade at different prices.

This software is meant to run 24/7 on a server that will monitor the market and trade for you.

It can operate in over a hundred exchanges around the world at the same time (uses [CCXT](https://github.com/ccxt/ccxt)) and the full list of supported exchanges can be found on their site. You will need to have accounts on at least two of them so arbitrage can take place.

## Installation and Configuration
Installation and configuration instructions are in the file [**install.md**](install.md).

## General logic of the application
Once you enter your credentials for at least two exchanges in the application, the system will only work with those markets.

It will display your current balance in different cryptocurrency.
It will also be constantly checking the books in all the different exchanges to see the current offers to buy or sell cryptocurrency and well as the last traded price.

Oficial exchange rates accross fiat currencies (EUR, USD, etc) are downloaded and used automatically.

## Example
Lets view a simple example:

* On Exchange A you can buy 1.300LTC for 200€ with a 0.015LTC comission
* On Exchange B you can sell 0.500LTC for 210€ with a 0.025LTC comission
Transfering funds from A to B has a comission of 0.010LTC

The system will then do the following:

1) Register the opportunity on the log/database and execute the operation if it has enough funds to do so and its above the configured minimum gain thresholds.

2) Send two simultanesos orders: 

* Buy 0.505TC for 200€ each in Exchange A
* Sell 0.500LTC for 210€ each in Exchange B

In will operate the minimum value of the two (0.500LTC) plus the total comissions it will need for the operation (0.015LTC + 0.025 LTC + 0.010 LTC = 0.050).


3) Wait for the operations to complete. Once confirm, it will log the operation in the logs.

4) Transfer the bought LTC from A to B to leave balances like before.

The financial gain in this example be the result of the following equation:

Total Gain = Selling Price - Purchase Price - Selling Comission - Buying Comission - Transfer Comission

| Selling Income | Purchase Cost   | Purchase Commision   | Selling Comission  | Transfer Commission   | Total Gain |
|---|---|---|---|---|---|
|0.500LTC \* 210€ = 105€|0.505LTC * 200€ = 101€|0.015LTC|0.025LTC|0.010LTC| 4€ and 0LTC  |

In the above example my LTC funds will remain the same in both exchanges and my € funds would have increased in 4€ in the Exchange B.


## Logs
All oportunities detected and trades completed are logged in a MySQL database and are printed out on the console (so can also be written to local text files).

An example of a text log file can be found [here](example_log_file.txt).


## Webserver
Mercury starts a web server on port 3000 where you can view you current balance on all exchanges, your open orders and your latest trades.

## ATM
ATM stands for **Automatic Trading Machine**. It's the module that will trade for you automatically when the conditions that you set are met. It's formed by 3 stages

### *Stage 1. ATM_Evaluate*
ATM_Evaluate is in charge of evaluating a possible operation to later execute it if the following conditions are met

1. Gain is over a certain %
2. Final destination is an acceptable FIAT
3. Balance is available in the selling side and buying side

It returns an object of status and reason. For example:

* `["APPROVED",'']` if approved.

* `["DENIED", 'Low gain']` if denied


### *Stage 2. ATM_Prioritize*
ATM_Prioritize will order my candiates to operate in order of most profitable

### *Stage 3. ATM_Execute*
ATM_Execute executes my best operation. 
It has two approaches:

#### ATM_Execute_Instant
Execute instantly with Market Price. This produces a trade using available offers of othertraders in the order book. This is the fastest type of trade

#### ATM_Execute_Limit
Execute instantly with Limit Price

If no instant trading is possible, because the margin is too low, mercury won't trade inmediatly but can enter offers in the order book to see if are accepted in a few seconds or minutes.

Let's say for example, I configured mercury to only trade if it can make more than 1%, and currently I can buy for 100€ and sell for 101.5€ with total fees of 1€.  If I would trade, I would only make 0.5€ (101.5 - 100 - 1)which is lower than the 1% thershold.  So instant trade wont occur. Instead I can make an offer.  The system will make an buy order of 99.5€ and a sell order of 102€.

If either of the two orders get accepted, the other will get canceled and executed at instant price (taker).

So if I manage to sell at 102€, I can purchase at 100€ and make my 1% profit.

If I manage to buy at 99.5€ I can sell at only 101.5€ and make my 1% profit also.

Trading operations take miliseconds and if you are unlucky things can change in that timeframe. This is why the minimum profit should not be 0.000000001%, but instead a minimum amount that can handle a small margin of error. Once an operation has started to execute (buy or sell), the other conterpart operation (sell or buy) will execute regardless of the final profitability.

## Manual trading

If the system detects and opportunity but can't operate automatically for some reason it will send you an email with the detected opportunity, and potential gain so you can trade manually if you wish.

This can occure for example if I a price difference between markets is detected but there are insufficient funds to complete the desired operation. So the system asks you to move funds so it can trade for you.
