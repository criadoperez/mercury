# Installation and Configuration instructions

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Installation](#installation)
- [Configure your API Keys for exchanges (Required)](#configure-your-api-keys-for-exchanges-required)
- [Configure parameters in original/arbitradge.js (All Optional)](#configure-parameters-in-originalarbitradgejs-all-optional)
  - [trading](#trading)
  - [baseFiat](#basefiat)
  - [fiats](#fiats)
  - [ATMMaximumReasonableGainThreshold](#atmmaximumreasonablegainthreshold)
  - [ATMGainThreshold](#atmgainthreshold)
  - [ATMGainThresholdInstant](#atmgainthresholdinstant)
  - [listOfAcceptableSymbols](#listofacceptablesymbols)
  - [RefrefeshRates](#refrefeshrates)
  - [E-mail](#e-mail)
  - [Database](#database)
  - [Exchange fees (Important)](#exchange-fees-important)
  - [Web server password](#web-server-password)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Installation

Steps to install:

1. Install `npm`, `nodejs` and `git`

        sudo apt install npm nodejs git

2. Clone the repository in your home folder

        cd
        git clone git@gitlab.com:criadoperez/mercury

2. Go to the mercury folder. Install npm dependencies from the mercury base folder. The output may produce some warnings which you can safely ignore.

        cd mercury/original
        sudo npm install

3. That's it! To run it type return to the project folder and run the start script. The log of whats going on behind the scenes (trading etc) will be logged in the *log* folder.

        cd ..
        ./run_mercury.sh

If you don't setup a MySQL database it will give you a warning that it can't find it. Using MySQL to store trades and opportunities is completely optional.

Before you run it you will probably want to configure it to fit your needs, following the steps below.

## Configure your API Keys for exchanges (Required)
In order to give Mercury capacity to operate it needs to access your exchanges accounts (at least two of them).

The file **/.env** contains this information. Edit accordignly. Most exchanges require the *apiKey* and the *secret*. Some also require the *uid*.

## Configure parameters in original/arbitradge.js (All Optional)
The following parameters can be left be default or configured to better suit your needs.

### trading
Default value: false

If trading is set to false, the application will NOT make any transactions. I will simply detect and log potential opportunities.
It is recommend you leave this set you false until you are confident enough to start trading with real money.

### baseFiat
By default baseFiat is the Euro, and it's set in the variable baseFiat.

BaseFiat should be the currency you measure your gains in, as this is the one the application will aim to increase.

You can change this by editing the following line to the 3 digit code of your currency. If your example you want to change from Euro to US Dollar you will change:

`var baseFiat = "EUR";`

to

`var baseFiat = "USD";`

### fiats
Default value: ["USD", "AUD", "EUR", "ZAR", "IDR", "GBP", "CAD", "JPY", "CZK", "ARS", "MYR", "HKD"]
Variable fiats contains all the fiat markets you wish to operate in. You may for example buy bitcoins in Euros and sell them in US dollars.

You can configure the fiat currencies the app is allowed to operate by editing the fiats variable.

### ATMMaximumReasonableGainThreshold

Default value: 2.00

Lower or equal to this gain it will consider it an opportunity. If gain is ridiculously high, it could be wrong.

### ATMGainThreshold

Default value: 1.005

Higher or equal to this gain it will consider it an opportunity.

1.0005 means 0.05% gain

### ATMGainThresholdInstant

Default value: 1.005

Higher or equal to this gain, it will operate inmediatly at market price
1.0005 means 0.05% gain

### listOfAcceptableSymbols
Default value:

    ["BTC/EUR","BTC/AUD","BTC/HKD","BCH/EUR","BCH/AUD","BCH/HKD","LTC/EUR","LTC/AUD","LTC/HKD","XRP/EUR","XRP/AUD","XRP/HKD"]

### RefrefeshRates
Several refresrates can be configured, all measured in seconds.

Default values are:

        refreshRate = 180
        tickersRefreshRate = 180
        executeStrategyRefreshRate = 5
        fiatDataRefreshRate = 3600
        balancesRefreshRate = 10
        analysisViewRefreshRate = 5

### E-mail
If you want to receive email alerts you will need to enter a valid Google email address that the system will use to send them.

This is used to inform you about trades that have just been completed (with the gain you made) and opportunites the system has detected but can't opperate itself.

Currently only gmail servers supported (both gmail.com domains or other Google Apps domains)

        //SET UP EMAIL
        var transporter = nodemailer.createTransport({
          service: 'gmail',
          auth: {
            user: 'user@domain.com',
            pass: 'password'
          }
        });

Email configuration just also be changed in the *email.js* file (as well as in the *arbitradge.js* file)

### Database
All detected opportunites and completed trades are logged in a MySQL database which you will need to setup: Defaults are:

        //SET UP DB CONNECTION
        var mysql = require('mysql');
        var con = mysql.createConnection({
          host: "localhost",
          user: "mercuryDB",
          password: "password",
          database: "mercury"
        });


### Exchange fees (Important)
Fees usually vary between exchanges and the amount of volume you have with that exchange. Mercury will only trade if it can make a profit *after* paying the fees, so it's important you keep your fees up to date so the system can make correct decision of wheter to trade or not.

For the moment we have one fee for all operations, but the application is ready to add a different fee per operation. For example in the same exchange the fee for BTC/EUR can be different to XRP/EUR. Also the *taker* fee is sometimes different to the *maker* fee.

These fees are entered in the .env file. Please enter your personal fees.

        # FEES
        # fee_all refers to same fee for taker and maker
        # 1.023 means 2.3%
        # Some exchanges my have a different maker and taker fee. We recommend to enter the highest
        bitstamp_fee_all=1.0024
        btcmarkets_fee_all=1.005
        coinspot_fee_all=1.009
        luno_fee_all=1.006
        coinmate_fee_all=1.0007
        kraken_fee_all=1.0026
        acx_fee_all=1.002
        coinbase_fee_all=1.0149
        gatecoin_fee_all=1.0035
        dsx_fee_all=1.0035

### Web server password
For security the web server asks for credentials to load the website.
Default credentials are:

> Username: mercury

> Password: mercurypass

This credentional can be changed in the Web Server Section of the arbitradge.js file

## Troubleshooting

Having problems? Suggestions for improvements? Your lucky, this is Free and Open Source software! 

Modifications, improvements and Pull Requests are welcome.
