import React, { Component } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';

const columns = [{
  dataField: 'name',
  text: 'Exchange',
  sort: true,
  filter: textFilter()
}, {
  dataField: 'fee',
  text: 'Fee',
  sort: true
}, {
  dataField: 'operation',
  text: 'Operation',
  sort: true
}];


class FeeTable extends Component {
    constructor(props) {
        super(props);
        this.state = {data: ["hello", "hello2"]};
      }
      componentDidMount(){
        fetch('http://localhost:8080/api/fees')
        .then(response => response.json())
        .then((results) => {
          this.setState({ data: results });
          console.log(results)})
        .catch(error => console.log(error))
      }
    render() {
    return (
        //<p> {this.state.data} </p>
        <BootstrapTable keyField='id' data={ this.state.data } columns={ columns } />
    );
  }
}

export default FeeTable;
