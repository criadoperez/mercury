#!/bin/bash
# A simple sheel script that stops any running instances (if any) and runs a new one,
# storing the output in a log file
# Please rename the folder names accordignly to where the app is and where you want the log file to be stored
killall node 2>/dev/null
# Create the log folder if it doesn exist
mkdir -p logs
# Run Mercury storing output in log file
/usr/bin/node ./original/arbitradge > ./logs/log_$(date +%Y-%m-%d-%H_%M_%S).txt &

