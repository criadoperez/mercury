'use strict';

var url = require('url');

var Default = require('./DefaultService');

module.exports.createOrder = function createOrder (req, res, next) {
  Default.createOrder(req.swagger.params, res, next);
};

module.exports.getBalances = function getBalances (req, res, next) {
  Default.getBalances(req.swagger.params, res, next);
};

module.exports.getFees = function getFees (req, res, next) {
  Default.getFees(req.swagger.params, res, next);
};

module.exports.getTickers = function getTickers (req, res, next) {
  Default.getTickers(req.swagger.params, res, next);
};
