//THIS FILE COINTAINS ALL MY EXCHANGES OBJECTS

const ccxt = require ('ccxt');

   /*******************
    *   ADD EXCHANGES
    *******************/ 
    //The Following API has permissions to operate on the API subaccount of Bitstamp
    //const bitstamp = new ccxt.bitstamp();
    const bitstamp = new ccxt.bitstamp(
        {
        //key limited to IP of server MAIN ACCOUNT - Full access
        apiKey: process.env.BITSTAMP_APIKEY,
        secret: process.env.BITSTAMP_SECRET,
        uid: process.env.BITSTAMP_UID
        });


//BTCMARKETS requires to secret to be base64 encoded.
const btcmarkets = new ccxt.btcmarkets({
            apiKey: process.env.BTCMARKETS_APIKEY,
            secret: process.env.BTCMARKET_SECRET
        });

const kraken = new ccxt.kraken(
    {//key descriptiion in kraken.com "1 FIRST API"
        apiKey: process.env.KRAKEN_APIKEY,
        secret: process.env.KRAKEN_SECRET
    });

const luno = new ccxt.luno({
         apiKey: process.env.LUNO_APIKEY,
         secret: process.env.LUNO_SECRET
          });

const coinspot = new ccxt.coinspot(
    {
        apiKey: process.env.COINSPOT_APIKEY,
        secret: process.env.COINSPOT_SECRET
    });

const coinmate = new ccxt.coinmate({
	    apiKey: process.env.COINMATE_APIKEY,
        secret: process.env.COINMATE_SECRET,
        uid: process.env.COINMATE_UID
     });


/* 
//OTHER PENDING EXCHANGES FOR THE FUTURE
//SatoshiTango doesnt seem to want to load, so this disabled
//const satoshitango = new ccxt.satoshitango();  //SATOSHITANGO does seem to have the option to create an API
//exchanges.push(satoshitango);
//const gdax = new ccxt.gdax()
//exchanges.push(gdax)
//const bitfinex = new ccxt.bitfinex ();
//exchanges.push(bitfinex);
//const bitbay = new ccxt.bitbay();
//exchanges.push(bitbay);

COINBASE IS CURRENTLY NOT SUPPORTED BY CCXT. Waiting for implementation
const coinbase = new ccxt.coinbase({ //Coinbase key has UNLIMITED access to the account.
        //Key limited to the IP of the server
apiKey: 'xxxxx',
secret: 'yyyyy'
});
exchange.push(coinbase);

COINEX currently not supported by ccxt
const coinex = new ccxt.coinex({
    apiKey: 'xxx',
    secret: 'yyy'
});
exchange.push(coinex)
*/

module.exports = {
    bitstamp: bitstamp,
    btcmarkets: btcmarkets,
    kraken: kraken,
    luno: luno,
    coinspot: coinspot,
    coinmate: coinmate,
};
