import React, { Component } from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

import 'react-bootstrap-table/dist/react-bootstrap-table.min.css';



const ccxt = require ('ccxt');
const fetch = require("node-fetch");

//Parameters
var baseFiat = "EUR"
var fiats = ["USD", "AUD", "EUR", "ZAR", "IDR"]
var baseCryptocurrency = "BTC"
var symbols = ["BTC", "ETH", "BCH","LTC", "XRP"] //List of accepted cryptocurrencies

//FIAT HANDLER

var fiatData ={"base":"EUR","date":"2018-01-15","rates":{"AUD":1.5434,"BGN":1.9558,"BRL":3.9237,"CAD":1.5263,"CHF":1.1799,"CNY":7.904,"CZK":25.531,"DKK":7.4496,"GBP":0.89043,"HKD":9.605,"HRK":7.434,"HUF":308.9,"IDR":16337.0,"ILS":4.1737,"INR":77.98,"JPY":135.81,"KRW":1305.7,"MXN":23.12,"MYR":4.8568,"NOK":9.6708,"NZD":1.6828,"PHP":61.753,"PLN":4.1686,"RON":4.6278,"RUB":69.283,"SEK":9.8335,"SGD":1.6223,"THB":39.194,"TRY":4.6442,"USD":1.2277,"ZAR":15.103}}

function updateFIATData(base){
    baseFiat = base
    const url = "https://api.fixer.io/latest?base="+base
    return fetch(url)
    .then(response => {
      response.json().then(json => {
        fiatData = json
      })
    })
    .catch(error => {
      console.log(error);
    });
}

function fiatRate(base, to){
    if(base==to) return 1
    return fiatData.rates[to]
}

function crpytoRate(baseExchange = baseExchange, base, to){
    if(baseExchange.symbols.indexOf(base+"/"+to)>-1) {
        //await baseExchange.fetchTicker(base+"/"+to)).then
    }
    else if (baseExchange.symbols.indexOf(to+"/"+base)>-1){
        //await baseExchange.fetchTicker(to+"/"+base))
    }
    return false
}


function fetchSymbols(exchange, symbol){
    try{
        var symbols = []
        exchange.loadMarkets()
        for(var i=0;i<exchange.symbols.length;i++){
            let s = exchange.symbols[i].split('/')
            if(s[0]==symbol || s[1]==symbol){
                symbols.push(exchange.symbols[i])
            }
        }
        return symbols
    }
    catch(error){
        return error
    }
}

function calculateGains(ticker1, ticker2){
    if(ticker1[baseFiat] != null && ticker2[baseFiat] != null) return ticker2[baseFiat]/ticker1[baseFiat]
}
var tickersHtml
var tickers2

function priceFormatter(cell, row){
    return '<i class="glyphicon glyphicon-usd"></i> ' + cell;
  }

class ArbitradgeTable extends Component {

    constructor(props) {
        super(props);
        this.state = {
          html: "",
        };
      }

      
    async componentDidMount(){
        console.log("--------------- START --------------------")
        
            //Get FIAT DATA & Test
            console.log("Rate from EUR to AUD")
            console.log(fiatRate("EUR", "AUD"))
        
            var tableify = require('tableify');
            var ratesHtml = tableify(fiatData.rates)
        
            // ADD EXCHANGES
            var exchanges = [] 
            const bitstamp = new ccxt.bitstamp()
            bitstamp.proxy="http://localhost:8080/"
            exchanges.push(bitstamp)
            const btcmarkets = new ccxt.btcmarkets()
            //exchanges.push(btcmarkets)
            const bitfinex = new ccxt.bitfinex ()
            bitfinex.proxy="http://localhost:8080/"
            exchanges.push(bitfinex)
            const bitbay = new ccxt.bitbay()
            //exchanges.push(bitbay)
            const kraken = new ccxt.kraken()
            //exchanges.push(kraken)
            const luno = new ccxt.luno()
            //exchanges.push(luno)
            const coinspot = new ccxt.coinspot()
            //exchanges.push(coinspot)
            const gdax = new ccxt.gdax()
            //exchanges.push(gdax)
            const coinmate = new ccxt.coinmate()
            //exchanges.push(coinmate)
        
            var data
        
            //console.log(await exchanges[0].fetchTickers())
        
            // LOAD MARKETS
            for (var i=0;i<exchanges.length;i++){
                await(exchanges[i].loadMarkets()).catch((error)=>{return error})
            }
            
            var tickers = []
            for (let exchange of exchanges){
                let listOfSymbols = exchange.symbols
                console.log(listOfSymbols)
                for(var i=0; i<listOfSymbols.length; i++){
                    let ticker = await (exchange.fetchTicker(listOfSymbols[i])).then(
                        (ticker) => {
                            let customTicker = {
                                exchange: exchange.name,
                                symbol: ticker.symbol,
                                symbolFrom: ticker.symbol.split('/')[0],
                                symbolTo: ticker.symbol.split('/')[1],
                                last: ticker.last,
                                [baseFiat]: ticker.last/fiatRate(baseFiat, ticker.symbol.split('/')[1]),
                                [baseCryptocurrency]: -1
                            }
                            tickers.push(customTicker)
                            console.log(exchange.name)
                            console.log(ticker.symbol)
                            console.log(ticker.last)
                        }
                    ).catch((error)=>{
                        console.log(error)
                        return error})
                    //Return tickers
                } 
            }
            tickers2 = []
            for (let exchange of exchanges){
                for(let baseTicker of tickers){
                    for(let endTicker of tickers){
                        if(baseTicker.exchange != endTicker.exchange && (baseTicker.symbol.includes(endTicker.symbolFrom) || baseTicker.symbol.includes(endTicker.symbolFrom))){
                            let customTicker = {
                                baseExchange: baseTicker.exchange,
                                baseSymbol: baseTicker.symbol,
                                baseLast: baseTicker.last,
                                baseVirtual: baseTicker[baseFiat],
                                endExchange: endTicker.exchange,
                                endSymbol: endTicker.symbol,
                                endLast: endTicker.last,
                                endVirtual: endTicker[baseFiat],
                                gain: endTicker[baseFiat]/baseTicker[baseFiat]
                            }
                            tickers2.push(customTicker)
                        }
                    }
                }
            }
            tickers2.sort(function(a,b) {
                if(a.gain > b.gain) return -1
                return 1
            })
        
            console.log("GENERATED {0} COMBINATIONS!!", tickers2.length)
            tickersHtml = tableify(tickers2)
            this.setState({html: tickersHtml})
    }
    render(){
        return(
        <BootstrapTable data={tickers2} striped={true} hover={true}>
            <TableHeaderColumn dataField="baseExchange" isKey={true} dataAlign="center" dataSort={true}>Base Ticker</TableHeaderColumn>
            <TableHeaderColumn dataField="baseSymbol" dataSort={true}>Symbol</TableHeaderColumn>
            <TableHeaderColumn dataField="baseLast" dataSort={true}>Last</TableHeaderColumn>
            <TableHeaderColumn dataField="endExchange" dataSort={true}>End Exchnage</TableHeaderColumn>
            <TableHeaderColumn dataField="endSymbol" dataSort={true}>Symbol</TableHeaderColumn>
            <TableHeaderColumn dataField="endLast" dataSort={true}>last</TableHeaderColumn>
            <TableHeaderColumn dataField="gain" dataFormat={priceFormatter}>Gain</TableHeaderColumn>
        </BootstrapTable>
        )
    }
}

export default ArbitradgeTable;